function [stats, hist2d] = phi_statistics(d, D, phi, max_phi)
% PHI_STATISTICS computes statistical measures on angular deviation

if nargin<4
    max_phi = 25;
end

% statistical measures: mean, median, RMSE, 95th percentile
stats = zeros(size(d,1), 4);

bin_edges = 0:.2:max_phi;
if nargout > 1
    hist2d = zeros(length(bin_edges)-1, size(d,1));
end

for k=1:length(d)
    idx = abs(D(:))<d(k) & ~isnan(phi(:));
    stats(k,1) = mean(phi(idx));
    stats(k,3) = sqrt(mean(phi(idx).^2));
    stats(k,[2 4]) = prctile(phi(idx),[50 95]);
    if nargout > 1
        hist2d(:,k) = histcounts(phi(idx), bin_edges, 'Normalization', 'probability');
    end
end