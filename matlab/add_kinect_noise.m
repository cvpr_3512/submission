function z = add_kinect_noise(z, mask)
% ADD_KINECT_NOISE adds a Kinect-like noise pattern to a synthetic depth image
%
% z = ADD_KINECT_NOISE(z)
% z = ADD_KINECT_NOISE(z, mask)
%
% Input:
% - z: synthetic depth image
% - mask (optional): values to which noise shall be added, defaul: all >0
%
% Output:
% - z: noisy depth image, same size as original one
%
% Noise model follows Khoshelham et al. 2012

if nargin<2
    mask = z>0;
end

sz = size(z);

% Compute normalized disparity d
% Eq. (5) and experiments:
% in cm: z^-1 = -2.85e-5 * d + 0.03
% in m:  z^-1 = -2.85e-3 * d + 3
%           d = (3 - z^-1) / 2.85e-3
d = zeros(sz);
d(mask) = (3 - 1./z(mask)) / 2.85e-3;

% Add noise to normalized disparity
% sigma_d = 0.5 pixels (p. 1450)
tmp = 0.5 * randn(sz);
d(mask) = d(mask) + tmp(mask);

% Add quantization to normalized disparity
d = round(d);

% Go back to z^-1 and z
z_inv = -2.85e-3 * d + 3;
z(mask) = 1 ./ z_inv(mask);