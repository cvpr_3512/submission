#ifndef RIGID_POINT_OPTIMIZER_H_
#define RIGID_POINT_OPTIMIZER_H_

// includes
#include <iostream>
//class includes
#include "RigidOptimizer.h"

/**
 * class declaration
 */
class RigidPointOptimizer : public RigidOptimizer {

public:

// constructors / destructor

    RigidPointOptimizer(Sdf* tSDF) :
        RigidOptimizer(tSDF)
    {}
    
    RigidPointOptimizer(int num_iterations, float conv_threshold, float damping, Sdf* tSDF) :
        RigidOptimizer(num_iterations, conv_threshold, damping, tSDF)
    {}
    
    ~RigidPointOptimizer() {}
    
    bool optimize_sampled(const cv::Mat &depth, const Mat3f K, size_t sampling);
    
// member functions
    
    bool optimize(const cv::Mat &depth, const Mat3f K) {
        // tSDF_->increase_counter();
        return optimize_sampled(depth, K, 1);
    }

};

#endif // RIGID_POINT_OPTIMIZER_H_