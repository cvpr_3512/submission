#ifndef IMG_LOADER_H_
#define IMG_LOADER_H_

// convenience header that includes all relevant image loaders

#include "img_loader/TumrgbdLoader.h"
#include "img_loader/Printed3dLoader.h"
#include "img_loader/SynthLoader.h"
#include "img_loader/RedwoodLoader.h"

#endif // IMG_LOADER_H_