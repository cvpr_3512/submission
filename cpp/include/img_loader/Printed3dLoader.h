#ifndef PRINTED3D_LOADER_H_
#define PRINTED3D_LOADER_H_

#include <iomanip>
#include "ImageLoader.h"

class Printed3dLoader : public ImageLoader {

private:

    size_t counter;

public:

    Printed3dLoader() :
        ImageLoader(1./1000, true),
        counter(0)
    {}
    
    Printed3dLoader(const std::string& path) :
        ImageLoader(1./1000, path, true),
        counter(0)
    {}
    
    ~Printed3dLoader() {}
    
    bool load_next(cv::Mat& color, cv::Mat& depth) {

        std::stringstream ss;
        ss << std::setfill('0') << std::setw(6) << counter;

        timestamp_rgb_ = ss.str();
        timestamp_depth_ = timestamp_rgb_;

        const std::string filename = timestamp_rgb_ + ".png";
    
        if (!load_depth("depth_" + filename, depth))
            return false;
        
        if (!load_color("color_" + filename, color))
            return false;
        
        ++counter;
        
        return true;
    }
    bool load_keyframe(cv::Mat &color, cv::Mat &depth, const int frame)
    {
        
        std::stringstream ss;
        ss << std::setfill('0') << std::setw(6) << frame;

        timestamp_rgb_ = ss.str();
        timestamp_depth_ = timestamp_rgb_;

        const std::string filename = timestamp_rgb_ + ".png";
        timestamps_depth_.push_back(timestamp_depth_);
        timestamps_rgb_.push_back(timestamp_rgb_);
    
        if (!load_depth("depth_" + filename, depth))
            return false;
        
        if (!load_color("color_" + filename, color))
            return false;
        
        
        return true;
    }

    void reset()
    {
        counter = 0;
        timestamps_depth_.clear();
        timestamps_rgb_.clear();
    }

};

#endif // PRINTED3D_LOADER_H_
