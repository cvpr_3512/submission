#ifndef SHARP_DETECTOR_H_
#define SHARP_DETECTOR_H_

#include <opencv2/core/core.hpp>

float modifiedLaplacian(const cv::Mat& src);

bool sharpDetector(const cv::Mat& img, float threshold){
    float measure = modifiedLaplacian(img);
    std::cout << "the sharpness measure is " << measure << "." << std::endl;
    if( measure < threshold)
        return false;
    return true;
}


// OpenCV port of 'LAPM' algorithm (Nayar89)
float modifiedLaplacian(const cv::Mat& src)
{
    cv::Mat M = (cv::Mat_<float>(3, 1) << -1, 2, -1);
    cv::Mat G = cv::getGaussianKernel(3, -1, CV_32F);

    cv::Mat Lx;
    cv::sepFilter2D(src, Lx, CV_32F, M, G);

    cv::Mat Ly;
    cv::sepFilter2D(src, Ly, CV_32F, G, M);

    cv::Mat FM = cv::abs(Lx) + cv::abs(Ly);

    float focusMeasure = cv::mean(FM).val[0];
    return focusMeasure;
}

#endif // SHARP_DETECTOR_H_