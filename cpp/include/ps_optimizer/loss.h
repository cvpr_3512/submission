#ifndef LOSS_H_
#define LOSS_H_

enum LossFunction
{
    L2 = 0,
    CAUCHY = 1,
    HUBER = 2,
    TUKEY = 3,
    TRUNC_L2 = 4
};

#endif //LOSS_H_